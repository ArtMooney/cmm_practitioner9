const io = require ('../io');
const crypt = require ('../crypt')
const requestJson = require ('request-json');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechucmm9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


// LOGIN Y LOGOUT v1: VERSION DEL PROFESOR:
function loginV1(req, res) {
 console.log("POST /apitechu/v1/login");

 var users = require('../usuarios.json');
 for (user of users) {
   if (user.email == req.body.email
       && user.password == req.body.password) {
     console.log("Email found, password ok");
     var loggedUserId = user.id;
     user.logged = true;
     console.log("Logged in user with id " + user.id);
     io.writeUserDataToFile(users);
     break;
   }
 }

 var msg = loggedUserId ?
   "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";

 var response = {
     "mensaje": msg,
     "idUsuario": loggedUserId
 };

 res.send(response);
}

function logoutV1(req, res) {
 console.log("POST /apitechu/v1/logout");

 var users = require('../usuarios.json');
 for (user of users) {
   if (user.id == req.params.id && user.logged === true) {
     console.log("User found, logging out");
     delete user.logged
     console.log("Logged out user with id " + user.id);
     var loggedoutUserId = user.id;
     io.writeUserDataToFile(users);
     break;
   }
 }

 var msg = loggedoutUserId ?
   "Logout correcto" : "Logout incorrecto";

 var response = {
     "mensaje": msg,
     "idUsuario": loggedoutUserId
 };

 res.send(response);
}

// LOGIN LOGOUT v2 VERSION PRACTICA:
function loginV2(req, res) {
 console.log("POST /apitechu/v2/login");

 var httpClient = requestJson.createClient (baseMlabURL);
 console.log("Client created");

 var loginEmail = req.body.email
 var loginPassw = req.body.password
 console.log("email de login: " + loginEmail)
 var query = 'q={"email": "' + loginEmail + '"}';

 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (err) {
       var response ={
         "msg" : "Error obteniendo usuario."
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         console.log ("Passsword introducida: " + loginPassw + "   Password Recuperada: " + body[0].password);
         if (crypt.checkPassword (loginPassw, body[0].password)) {
           console.log ("Usuario y pasword correctos.");

         }
         var response = body[0];
       } else {
         var response = {
           "msg" : "Usuario no encontrado."
         };
         res.status(404);
       }
     }
     res.send (response)
   }
 )

}

function logoutV2(req, res) {
 console.log("POST /apitechu/v2/logout");

 }

// Export de las funciones del controlador.
module.exports.loginV1 = loginV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV1 = logoutV1;
module.exports.logoutV2 = logoutV2;



// LOGIN Y LOGOUT v1: VERSION ENTREGADA COMO SOLUCCION:
//
// function userLoginV1 (req, res) {
//   console.log('POST /apitechu/v1/login');
//
//   console.log(req.body.email);
//   console.log(req.body.password);
//
//   var users = require('../usuarios.json');
//
//   console.log("Usando Array findIndex");
//   var indexOfElement = users.findIndex(
//     function(element){
//       console.log("comparando " + element.email + " y " +   req.body.email);
//       return (element.email == req.body.email && element.password == req.body.password)
//     }
//   )
//
//   console.log("indexOfElement es " + indexOfElement);
//
//   var result = {};
//
//   if (indexOfElement >= 0 ) {
//     console.log("Login correcto, id de usuario: " + users[indexOfElement].id);
//     users[indexOfElement].logged = true;
//     io.writeUserDataToFile(users);
//     result = {"mensaje" : "Login correcto.", "idUsuario" : users[indexOfElement].id};
//   }
//    else {
//      console.log("Login incorrecto." + indexOfElement);
//      result = {"mensaje" : "Login incorrecto."};
//   }
//   res.send(result);
// }
//
// function userLogoutV1(req, res) {
//   console.log("POST /apitechu/v1/logout/:id");
//   console.log("id es " + req.params.id);
//
//   var users = require('../usuarios.json');
//   var loggedIn = false
//   var result = {};
//
//   console.log("Usando for normal");
//   for (var i = 0; i < users.length; i++) {
//      console.log("comparando " + users[i].id + " y " +  req.params.id);
//      if (users[i].id == req.params.id && users[i].logged) {
//        console.log("La posicion " + i + " coincide y está logeado.");
//        loggedIn = true;
//        delete users[i].logged
//        break;
//      }
//   }
//
//   if (loggedIn) {
//     result = {"mensaje" :  "logout correcto.", "idUsuario" : users[i].id };
//     io.writeUserDataToFile(users);
//   }
//   else {
//     result = {"mensaje" :  "logout incorrecto." };
//   }
//
//  res.send(result);
// }
