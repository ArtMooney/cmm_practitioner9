const io = require ('../io');
const crypt = require ('../crypt')
const requestJson = require ('request-json');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechucmm9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUsersV1 (req, res){
  console.log("GET /apitechu/v1/users");

  var result = {};
  var users = require('../usuarios.json');

  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }

result.users = req.query.$top ?
  users.slice(0, req.query.$top) : users;

res.send(result);
}

function getUsersV2 (req, res){
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient (baseMlabURL);
  console.log("Client created");

  httpClient.get("user?" + mLabAPIKey,
    function(err, resMLab, body) {
      var response = !err ? body : {
        "msg"  : "Error obteniendo usuarios."
      }
      res.send (response)
    }

  )
}

function getUsersByIdV2(req, res) {
  console.log("GET /apitechu/v2/users/:id");

  var httpClient = requestJson.createClient (baseMlabURL);
  console.log("Client created");

  var id = req.params.id;
  console.log("Id del usuario a traer: " + id);
  var query = 'q={"id":' + id + '}';

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        var response ={
          "msg" : "Error obteniendo usuario."
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body[0];
        } else {
          var response = {
            "msg" : "Usuario no encontrado."
          };
          res.status(404);
        }
      }
      res.send (response)
    }

  )
}

function createUserV1 (req, res) {
  console.log('POST /apitechu/v1/users');

  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "id" : req.body.id,
    "first_name": req.body.first_name,
    "last_name" : req.body.last_name,
    "email" :     req.body.email
  };

  var users = require('../usuarios.json');
  users.push(newUser);
  io.writeUserDataToFile(users);

}

function createUserV2 (req, res) {
  console.log('POST /apitechu/v2/users');

  console.log("id es: "+req.body.id);
  console.log("first_name es: "+req.body.first_name);
  console.log("last_name es: " + req.body.last_name);
  console.log("email es: " + req.body.email);
  console.log("password es: " + req.body.password);

  var newUser = {
    "id" : req.body.id,
    "first_name": req.body.first_name,
    "last_name" : req.body.last_name,
    "email" :     req.body.email,
    "password" :  crypt.hash(req.body.password)
  };

  var httpClient = requestJson.createClient (baseMlabURL);
  httpClient.post("user?" + mLabAPIKey, newUser,
    function (err, resMLab, body) {
      console.log("Usuario guardado con éxito.");
      res.status(201).send({"msg" : "Usuario guardado con éxito."})
    }
  );
}

function deleteUserV1(req, res) {
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id es " + req.params.id);

  var users = require('../usuarios.json');
  var deleted = false;

  // console.log("Usando for normal");
  // for (var i = 0; i < users.length; i++) {
  //   console.log("comparando " + users[i].id + " y " +  req.params.id);
  //   if (users[i].id == req.params.id) {
  //     console.log("La posicion " + i + " coincide");
  //     users.splice(i, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for in");
  // for (arrayId in users) {
  //   console.log("comparando " + users[arrayId].id + " y "  + req.params.id);
  //   if (users[arrayId].id == req.params.id) {
  //     console.log("La posicion " + arrayId " coincide");
  //     users.splice(arrayId, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for of");
  // for (user of users) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posición ? coincide");
  //     // Which one to delete? order is not guaranteed...
  //     deleted = false;
  //     break;
  //   }
  // }

  // console.log("Usando for of 2");
  // // Destructuring, nodeJS v6+
  // for (var [index, user] of users.entries()) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posicion " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for of 3");
  // var index = 0;
  // for (user of users) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posición " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //     break;
  //   }
  //   index++;
  // }

  console.log("Usando Array ForEach");
  users.forEach(function (user, index) {
    console.log("comparando " + user.id + " y " +  req.params.id);
  if (user.id == req.params.id) {
    console.log("La posicion " + index + " coincide");
    users.splice(index, 1);
    deleted = true;
  }
 });

 // Aproximación de MARIO para hacerloe n una sola línea.
 // users.splice(users.findIndex (user => user.id == req.params.id),1);

  // console.log("Usando Array findIndex");
  // var indexOfElement = users.findIndex(
  //   function(element){
  //     console.log("comparando " + element.id + " y " +   req.params.id);
  //     return element.id == req.params.id
  //   }
  // )
  //
  // console.log("indexOfElement es " + indexOfElement);
  // if (indexOfElement >= 0) {
  //   users.splice(indexOfElement, 1);
  //   deleted = true;
  // }

  if (deleted) {
    io.writeUserDataToFile(users);
 }

  var msg = deleted ?
  "Usuario borrado" : "Usuario no encontrado."

  console.log(msg);
  res.send({"msg" : msg});
}

// Export de las funciones del controlador.
module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersByIdV2 =getUsersByIdV2
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
