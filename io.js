const fs = require('fs');

function writeUserDataToFile(data) {
  console.log('witeUserDataToFile');
  var jsonData = JSON.stringify(data)

  fs.writeFile("./usuarios.json", jsonData, "utf8",
    function(err) {
      if (err) {
        console.log(err);
      } else {
        console.log("Usuario persistido.")
      }
    }
  )
}

// OJO: El export es importante para poder exportar la función.
module.exports.writeUserDataToFile = writeUserDataToFile;
