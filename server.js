require('dotenv').config();

const express = require('express');
const app = express();

const io = require ('./io');

const userController = require('./controllers/UserController')
const authController = require('./controllers/AuthController')


app.use (express.json());

const port = process.env.PORT || 3000;
app.listen(port);
console.log("SuperAPI escuchando en el puerto BIP BIP: " + port);

app.get('/apitechu/v1/hello',
function(req, res){
  console.log("GET /apitechu/v1/hello");

  res.send({"msg" : "Hola caracola desde API TechU."});
}
)

app.post('/apitechu/v1/monstruo/:para1/:para2',
  function(req, res){
    console.log("Parámetros:");
    console.log(req.params);

    console.log("Query String:");
    console.log(req.query);

    console.log("Headers:");
    console.log(req.headers);

    console.log("Body:");
    console.log(req.body);

  }
)

app.get('/apitechu/v1/users', userController.getUsersV1);
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v2/users/:id', userController.getUsersByIdV2);

app.post ('/apitechu/v1/users', userController.createUserV1);
app.post ('/apitechu/v2/users', userController.createUserV2);

app.delete('/apitechu/v1/users/:id',userController.deleteUserV1);

app.post('/apitechu/v1/login', authController.loginV1);
app.post('/apitechu/v2/login', authController.loginV2);

app.post('/apitechu/v1/logout/:id', authController.logoutV1);
app.post('/apitechu/v2/logout/:id', authController.logoutV2);
