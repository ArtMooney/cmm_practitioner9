const mocha = require('mocha');
const  chai =require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();

describe('Frist test',
  function() {
    it('Test that DUkcDuckgo works', function(done) {
  // Vamos a utilizar chaining para conseguir fluent interfaces
      // Test guay
      chai.request('http://www.duckduckgo.com')
      // Test malo
      // chai.request('https://developer.mozilla.org/en-US/adsfasdfas')
      .get('/')
      .end(
        function(err, res){
          // console.log(res);
          console.log("Request has finished");
          console.log(err);
          res.should.have.status(200);
          done();
        }
      )
    }

    )
  }
)

describe('Test de SuperAPI Usuarios',
  function() {
    it('Prueba que la API de Usuarios responde correctamente.', function(done) {
      chai.request('http://localhost:3000')
      .get('/apitechu/v1/hello')
      .end(
        function(err, res){
          console.log("Request has finished");
          res.should.have.status(200);
          res.body.msg.should.be.eql("Hola caracola desde API TechU.");
          done();
        }
      )
    }

  ),
  it('Prueba que la API devuelve una lista de usuarios correctos.', function(done) {
    chai.request('http://localhost:3000')
    .get('/apitechu/v1/users')
    .end(
      function(err, res){
        console.log("Request has finished");

        res.should.have.status(200);
        res.body.users.should.be.a('array');

        for (user of res.body.users){
          user.should.have.property('email');
          user.should.have.property('first_name');
        }
        
        done();
      }
    )
  }

  )
  }
)
